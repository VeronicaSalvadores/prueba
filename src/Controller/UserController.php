<?php

namespace App\Controller;

use App\Entity\Usuario;
use App\Form\UserFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController{

    /**
    * @Route("/users", name="app_users")
    */
    
    public function listUsers (EntityManagerInterface $em) {

        $repositorio = $em->getRepository(Usuario::class);
        $users = $repositorio->findAll();
  
        return $this->render("user.html.twig", ["users"=>$users]);

    }

    /**
    * @Route("/users/new", name="app_new")
    */

    public function newUser(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder){

        $usuario = new Usuario();

        $form = $this->createForm(UserFormType::class, $usuario);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $usuario = $form->getData();
            $usuario->setPassword($passwordEncoder->encodePassword($usuario, $form->get('password')->getData()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();

            return $this->redirectToRoute('app_users');
        }

        return $this->render('security/new.html.twig', ['userForm' => $form->createView()]);

    }

    /**
    * @Route("/users/{id}", name="detail_user")
    */
    
    public function detailUser($id, EntityManagerInterface $em) {
        
        $repositorio = $em->getRepository(Usuario::class);
        $user = $repositorio->find($id);

        return $this->render('detail-user.html.twig', ["user"=>$user]);

    }

    /**
    * @Route("/users/edit/{id}", name="edit_user")
    */

    public function editUser (int $id, Request $request, Usuario $usuario, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder){

        $em = $this->getDoctrine()->getManager();
        $usuario = $em->getRepository(Usuario::class)->find($id);

        $form = $this->createForm(UserFormType::class, $usuario);

        $form->handleRequest($request);

         if ($form->isSubmitted() && $form->isValid()) {

            $usuario->setPassword($passwordEncoder->encodePassword($usuario, $form->get('password')->getData()));
            $em->flush();

            return $this->redirectToRoute('app_users');
        }

        return $this->render('edit-user.html.twig', ['userForm' => $form->createView()]);

    }

    /**
    * @Route("/users/delete/{id}", name="remove_user")
    */

    public function deleteUser(Usuario $usuario, EntityManagerInterface $em){

        $em->remove($usuario);
        $em->flush();

        return $this->redirectToRoute('app_users');
    }


}