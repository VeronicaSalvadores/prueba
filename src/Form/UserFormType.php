<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array(
                'label' => ' ',
                'attr' => array(
                    'placeholder' => 'Usuario'
                )
           ))
            ->add('email', EmailType::class, array(
                'label' => ' ',
                'attr' => array(
                    'placeholder' => 'Email'
                )
           ))
            ->add('password', PasswordType::class, array(
                'label' => ' ',
                'attr' => array(
                    'placeholder' => 'Contraseña'
                )
           ))
            ->add('comentarios', null, array(
                'label' => ' ',
                'attr' => array(
                    'placeholder' => ' Comentarios'
                )
           ))
            ->add('Guardar', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}
